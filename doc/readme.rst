Overview
========

.. {# pkglts, glabpkg

.. image:: https://revesansparole.gitlab.io/transect_lidar/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://revesansparole.gitlab.io/transect_lidar/

.. image:: https://revesansparole.gitlab.io/transect_lidar/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/transect_lidar/0.0.1/

.. image:: https://revesansparole.gitlab.io/transect_lidar/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/transect_lidar

.. image:: https://badge.fury.io/py/transect_lidar.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/transect_lidar



main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/revesansparole/transect_lidar/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/revesansparole/transect_lidar/commits/main

.. |main_coverage| image:: https://gitlab.com/revesansparole/transect_lidar/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/revesansparole/transect_lidar/commits/main
.. #}

Set of tools to handle transecting with a lidar
